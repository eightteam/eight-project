<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Сайт для студентов</title>

  <link href="./css/style.css" rel="stylesheet">
  <!-- Bootstrap -->
  <link href="./css/bootstrap.min.css" rel="stylesheet">

  <script src="./js/jquery.min.js"></script>
  <script src="./js/jquery.dropotron.min.js"></script>
  <script type="text/javascript" src="./js/jquery-2.1.4.min.js"></script>
  <script type="text/javascript" src="./js/jquery.validate.min.js"></script>
  
</head>
<body>
  <!-- fixed navigation bar -->
<div class="navbar navbar-fixed-top navbar-inverse" role="navigation">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#b-menu-1">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="/">Сайт студентов для студентов</a>
      </div>
      
      <div class="collapse navbar-collapse" id="b-menu-1">
        <ul class="nav navbar-nav navbar-right">
        	
          <li><a href="/">Главная</a></li>
          <li class="active"><a href="/ask">Задать вопрос</a></li>
          <li><a href="/knowledge-base">База знаний</a></li>
          <li><a href="/rating">Рейтинг</a></li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-user"></span><b class="caret"></b></a>
            <ul class="dropdown-menu">
                <li><a href="/profile">Моя страница</a></li>
              <li><a href="/setting">Настройки</a></li>
              <li><a href="/logout">Выйти</a></li>
            </ul>
          </li>
        </ul>
      </div> <!-- /.nav-collapse -->
    </div> <!-- /.container -->
  </div> <!-- /.navbar -->

  <!-- 2-column layout -->
  <div class="container main-block">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Задать вопрос</h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <form class="q-form" id="ask_question" action="/ask" method="POST" enctype="multipart/form-data">
                            <div class="form-group">
                                <label for="q">Предмет:</label>
                                <select class="form-control" name="subjectName" id="subject" class="q-field" type="text" required autofocus>
                                 <#list model.subjects as subject>
                                <option>${subject.name}</option>
                                </#list>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="q">Заголовок:</label>
                                <input class="form-control" name="header" id="header" class="q-field" type="text" required autofocus />
                            </div>
                            <div class="form-group">
                                <label for="q-ta">Вопрос:</label>
                                <textarea name="content" id="content" class="form-control"></textarea>
                            </div>
                            <div class="form-group">
                                 <label for="q">Добавить картинку</label>
                                 <input type="file" name="file">
                            </div>
                            <input type="submit" onclick="ask_question()" class="btn btn-labeled btn-success" value="Задать вопрос">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
  var question_validator = $("#ask_question").validate({
    rules: {

      header: {
        required: true,
        minlength: 3,
        maxlength: 200
      },

      content: {
        required: true,
        minlength: 3,
        maxlength: 500
      }
    },

    messages: {
      header: {
        required: "Это поле обязательно для заполнения",
        minlength: "Заголовок должен содержать минимум 3 символа",
        maxlength: "Заголовок должен содержать максимум 200 символов"
      },

      content: {
        required: "Это поле обязательно для заполнения",
        minlength: "Вопрос должен содержать минимум 3 символа",
        maxlength: "Вопрос должен содержать максимум 500 символов"
      },
    }
  });

   function ask_question() {
    question_validator.form();
    if (question_validator.numberOfInvalids() == 0) {
      alert("Ваш вопрос успешно добавлен!")
    }
  }


</script>

<footer>
  <!-- Footer -->
</footer>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>
</body>
</html>