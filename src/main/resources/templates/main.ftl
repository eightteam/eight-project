<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Сайт для студентов</title>

  <link href="./css/style.css" rel="stylesheet">
  <!-- Bootstrap -->
  <link href="./css/bootstrap.min.css" rel="stylesheet">

  
</head>
<body>
  <!-- fixed navigation bar -->
  <div class="navbar navbar-fixed-top navbar-inverse" role="navigation">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#b-menu-1">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="/">Сайт студентов для студентов</a>
      </div>
      
      <div class="collapse navbar-collapse" id="b-menu-1">
        <ul class="nav navbar-nav navbar-right">
        	<#--<li>
        		<div id ="imaginary_container" class="input-group stylish-input-group">
            		<input type="text" class="form-control"  placeholder="Search question" >
            		<span class="input-group-addon">
                	<button type="submit">
                    	<span class="glyphicon glyphicon-search"></span>
                	</button>  
            		</span>
      			</div>
  			</li>-->
          <li class="active"><a href="/">Главная</a></li>
          <li><a href="/ask">Задать вопрос</a></li>
          <li><a href="/knowledge-base">База знаний</a></li>
          <li><a href="/rating">Рейтинг</a></li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-user"></span><b class="caret"></b></a>
            <ul class="dropdown-menu">
              <li><a href="/profile">Моя страница</a></li>
              <li><a href="/setting">Настройки</a></li>
              <li><a href="/logout">Выйти</a></li>
            </ul>
          </li>
        </ul>
      </div> <!-- /.nav-collapse -->
    </div> <!-- /.container -->
  </div> <!-- /.navbar -->

  <!-- 2-column layout -->
  <div class="container main-block">
  <div class="row row-offcanvas row-offcanvas-right">

    <!-- column 3 (sidebar) -->
    <div class="col-sm-3 sidebar-offcanvas" id="sidebar">
      <div class="list-group" role="navigation">
          <#list model.subjects as subject>
              <#assign modelSubject>${subject.engName}</#assign>
        <a href="/main/${modelSubject}" class="list-group-item">${subject.name}</a>
          </#list>
      </div>
    </div><!-- /column 3 (sidebar) -->

    <div class="col-xs-12 col-sm-9">

      <div class="row">
        <div class="alert alert-success">Выберите предмет из списка</div>
      </div><!--/row-->
    </div><!--/span-->


    </div><!--/row-->
    </div>

    <footer>
      <!-- Footer -->
    </footer>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="./js/bootstrap.min.js"></script>
  </body>
  </html>