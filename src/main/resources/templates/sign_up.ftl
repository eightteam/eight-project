<#ftl encoding='UTF-8'>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Сайт для студентов</title>

    <link href="./css/style.css" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="./css/bootstrap.min.css" rel="stylesheet">

    <script src="./js/jquery.min.js"></script>
    <script src="./js/jquery.dropotron.min.js"></script>
    <script src="./js/jquery-2.1.4.min.js"></script>
    <script src="./js/jquery.validate.min.js"></script>


</head>
<body>
<!-- fixed navigation bar -->
<div class="navbar navbar-fixed-top navbar-inverse" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#b-menu-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/">Сайт студентов для студентов</a>
        </div>

        <div class="collapse navbar-collapse" id="b-menu-1">
            <#--<ul class="nav navbar-nav navbar-right">

                <li><a href="index.ftl">Главная</a></li>
                <li><a href="ask.ftl">Задать вопрос</a></li>
                <li><a href="knowledge_base.ftl">База знаний</a></li>
                <li><a href="#">Рейтинг</a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-user"></span><b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="#">Настройки</a></li>
                        <li><a href="aut.html">Выйти</a></li>
                    </ul>
                </li>
            </ul>-->
        </div> <!-- /.nav-collapse -->
    </div> <!-- /.container -->
</div> <!-- /.navbar -->

<!-- 2-column layout -->
<div class="container main-block">
    <div class="row">
        <div class="col-xs-12 col-sm-10 col-lg-8 col-md-offset-2 col-lg-offset-2">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Регистрация</h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <form class="q-form" id="registration" action="/signUp" method="POST">
                        <#if error??>
                            <div class="alert alert-danger " role="alert">${error}</div>
                        </#if>
                            <div class="form-group">
                                <label for="q">Логин:</label>
                                <input class="form-control" name="login" id="login" class="q-field" type="text" required autofocus />
                            </div>
                            <div class="form-group">
                                <label for="q">Никнейм:</label>
                                <input class="form-control" name="nickname" id="nickname" class="q-field" type="text" required autofocus />
                            </div>
                            <div class="form-group">
                                <label for="q">Фамилия:</label>
                                <input class="form-control" name="secondName" id="surname" class="q-field" type="text" required autofocus />
                            </div>
                            <div class="form-group">
                                <label for="q">Имя:</label>
                                <input class="form-control" name="firstName" id="name" class="q-field" type="text" required autofocus />
                            </div>
                            <div class="form-group">
                                <label for="q">Отчество:</label>
                                <input class="form-control" name="lastName" id="patronymic" class="q-field" type="text" required autofocus />
                            </div>
                            <div class="form-group">
                                <label for="q">Группа:</label>
                                <input class="form-control" name="userGroup" id="group" class="q-field" type="text" required autofocus />
                            </div>
                            <div class="form-group">
                                <label for="q">Пароль:</label>
                                <input class="form-control" name="password" id="password1" class="q-field" type="password" required autofocus />
                            </div>
                            <div class="form-group">
                                <label for="q">Подтвердите пароль:</label>
                                <input class="form-control" name="confirmPassword" id="password2" class="q-field" type="password" required autofocus />
                            </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <div class="row">
                        <div class="col-xs-6 col-sm-6 col-md-6">
                                <input type="submit" onclick="registration()" class="btn btn-success" placeholder="Зарегистрироваться">
                        </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var validator = $("#registration").validate({

        rules: {

            login: {
                required: true,
                minlength: 4,
                maxlength: 16,
                regx: /^[A-Za-z0-9_-]{4,16}$/
            },

            nickname: {
                required: true,
                minlength: 4,
                maxlength: 16,
                regx: /^[A-Za-z0-9_-]{4,16}$/
            },

            name: {
                required: true,
                minlength: 1,
                maxlength: 30,
                regx: /^[А-Яа-я]{1,30}$/
            },

            surname: {
                required: true,
                minlength: 1,
                maxlength: 30,
                regx: /^[А-Яа-я]{1,30}$/
            },

            patronymic: {
                required: true,
                minlength: 1,
                maxlength: 30,
                regx: /^[А-Яа-я]{1,30}$/
            },

            group: {
                required: true,
                minlength: 1,
                maxlength: 30,
                regx: /^[0-9-_]{1,30}$/
            },

            password1: {
                required: true,
                minlength: 3,
                maxlength: 16,
                regx: /^[A-Za-z0-9_-]{3,16}$/
            },

            password2: {
                required: true,
                equalTo: "#password1"
            }

        },

        messages: {

            login: {
                required: "Это поле обязательно для заполнения",
                minlength: "Логин должен содержать минимум 4 символа",
                maxlength: "Логин должен содержать максимум 16 символов",
                regx: "Логин может содержать только буквы латинского алфавита, цифры, дефис и нижнее подчеркивание"
            },

            nickname: {
                required: "Это поле обязательно для заполнения",
                minlength: "Никнейм должен содержать минимум 4 символа",
                maxlength: "Никнейм должен содержать максимум 16 символов",
                regx: "Никнейм может содержать только буквы латинского алфавита, цифры, дефис и нижнее подчеркивание"
            },

            name: {
                required: "Это поле обязательно для заполнения",
                minlength: "Имя должно содержать минимум 1 символ",
                maxlength: "Имя должно содержать максимум 30 символов",
                regx: "Имя может содержать только русские буквы алфавита"
            },

            surname: {
                required: "Это поле обязательно для заполнения",
                minlength: "Фамилия должна содержать минимум 1 символ",
                maxlength: "Фамилия должна содержать максимум 30 символов",
                regx: "Фамилия может содержать только русские буквы алфавита"
            },

            patronymic: {
                required: "Это поле обязательно для заполнения",
                minlength: "Отчество должно содержать минимум 1 символ",
                maxlength: "Отчество должно содержать максимум 30 символов",
                regx: "Отчество может содержать только русские буквы алфавита"
            },

            group: {
                required: "Это поле обязательно для заполнения",
                minlength: "Номер группы должен содержать минимум 1 символ",
                maxlength: "Номер группы должен содержать максимум 30 символов",
                regx: "Номер группы может содержать только цифры, дефис и нижнее подчеркивание"
            },

            password1: {
                required: "Это поле обязательно для заполнения",
                minlength: "Пароль должен содержать минимум 3 символа",
                maxlength: "Пароль должен содержать максимум 16 символов",
                regx: "Пароль может содержать только буквы латинского алфавита, цифры, дефис и нижнее подчеркивание"
            },

            password2: {
                required: "Это поле обязательно для заполнения",
                equalTo: "Пароли не совпадают"
            }

        }

    });

    $.validator.addMethod("regx", function(value, element, regexpr) {
        return regexpr.test(value);
    }, "Error");

</script>

<footer>
    <!-- Footer -->
</footer>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>
</body>
</html>