<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Сайт для студентов</title>

  <link href="/../css/style.css" rel="stylesheet">
  <!-- Bootstrap -->
  <link href="/../css/bootstrap.min.css" rel="stylesheet">

  <script src="/../js/jquery.min.js"></script>
  <script src="/../js/jquery.dropotron.min.js"></script>
  <script type="text/javascript" src="/../js/jquery-2.1.4.min.js"></script>
  <script type="text/javascript" src="/../js/jquery.validate.min.js"></script>
</head>
<body>
  <!-- fixed navigation bar -->
  <div class="navbar navbar-fixed-top navbar-inverse" role="navigation">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#b-menu-1">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="/">Сайт студентов для студентов</a>
      </div>
      
      <div class="collapse navbar-collapse" id="b-menu-1">
        <ul class="nav navbar-nav navbar-right">
        	
          <li><a href="/">Главная</a></li>
          <li><a href="/ask">Задать вопрос</a></li>
          <li><a href="/knowledge-base">База знаний</a></li>
          <li><a href="/rating">Рейтинг</a></li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-user"></span><b class="caret"></b></a>
            <ul class="dropdown-menu">
              <li><a href="/profile">Моя страница</a></li>
              <li><a href="/setting">Настройки</a></li>
              <li><a href="/logout">Выйти</a></li>
            </ul>
          </li>
        </ul>
      </div> <!-- /.nav-collapse -->
    </div> <!-- /.container -->
  </div> <!-- /.navbar -->


  <!-- 2-column layout -->
  <div class="container main-block">
      <div class="row">
          <div class="col-xs-12 col-sm-12 col-lg-12">
              <div class="panel panel-primary">
                  <div class="panel-heading">
                      <h3 class="panel-title">Добавить материал по предмету</h3>
                  </div>
                  <div class="panel-body">
                      <div class="row">
                      <#assign engSubject>${model.subject}</#assign>
                          <form class="q-form" id="add_document" action="/add/${engSubject}" method="POST" enctype="multipart/form-data">
                              <div class="form-group">
                                  <label for="q">Заголовок:</label>
                                  <input class="form-control" name="header" id="header" class="q-field" type="text" required autofocus />
                              </div>
                              <div class="form-group">
                                  <label for="q-ta">Описание:</label>
                                  <textarea name="content" id="content" class="form-control"></textarea>
                              </div>
                              <div class="form-group">
                                  <label for="q">Вставьте ссылку:</label>
                                  <input class="form-control" id="insert_link" class="q-field" type="text" name="path" autofocus />
                              </div>
                              <div class="text-center">
                                  <label>или</label>
                              </div>
                              <div class="form-group">
                                  <label for="q">Прикрепить документ</label>
                                  <input type="file" name="file">
                              </div>
                              <input type="submit" onclick="add_document()" class="btn btn-labeled btn-success" value="Добавить">
                          </form>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </div>

<script type="text/javascript">
  var doc_validator = $("#add_document").validate({

    rules: {

        header: {
            required: true,
            minlength: 3,
            maxlength: 200
        },

      content: {
        required: true,
        minlength: 4,
        maxlength: 500
        /*regx: /^[А-Яа-яA-Za-z0-9-.,]{4,200}$/*/
      },

      insert_link: {
          required: false,
          minlength: 3,
          maxlength: 200
      }
    },

    messages: {

        header: {
            required: "Это поле обязательно для заполнения",
            minlength: "Заголовок должен содержать минимум 3 символа",
            maxlength: "Заголовок должен содержать максимум 200 символов"
        },

      content: {
        required: "Это поле обязательно для заполнения",
        minlength: "Описание должно содержать минимум 4 символа",
        maxlength: "Описание должно содержать максимум 200 символов",
        regx: "Описание может содержать только буквы, цифры, дефис, точку или запятую"
      },
      insert_link: {
          minlength: "Ссылка должна содержать минимум 3 символа",
          maxlength: "Ссылка должна содержать максимум 200 символов"
      }
    }

  });

  $.validator.addMethod("regx", function(value, element, regexpr) {
    return regexpr.test(value);
  }, "Error");

  function add_document() {
      doc_validator.form();
    if (doc_validator.numberOfInvalids() == 0) {
      alert("Ваш материал успешно добавлен")
    }
  }

  </script>

  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src="/../js/bootstrap.min.js"></script>
</body>
</html>