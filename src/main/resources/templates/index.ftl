<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Сайт для студентов</title>

  <link href="/../css/style.css" rel="stylesheet">
  <link href="/../css/bootstrap.min.css" rel="stylesheet">
  <link href="/../css/star.css" rel="stylesheet">

  <script type="text/javascript" src="/../js/jquery-2.1.4.min.js"></script>
  <script type="text/javascript" src="/../js/jquery.validate.min.js"></script>
  <script type="text/javascript" src="/../js/bootstrap.min.js"></script>
  <script type="text/javascript" src="/../js/star.js"></script>
</head>
<body>
  <!-- fixed navigation bar -->
  <div class="navbar navbar-fixed-top navbar-inverse" role="navigation">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#b-menu-1">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="/">Сайт студентов для студентов</a>
      </div>
      <div class="collapse navbar-collapse" id="b-menu-1">
        <ul class="nav navbar-nav navbar-right">
        	<#--<li>
        		<div id ="imaginary_container" class="input-group stylish-input-group">
            		<input type="text" class="form-control"  placeholder="Search question" >
            		<span class="input-group-addon">
                	<button type="submit">
                    	<span class="glyphicon glyphicon-search"></span>
                	</button>  
            		</span>
      			</div>
  			</li>-->
          <li class="active"><a href="/">Главная</a></li>
          <li><a href="/ask">Задать вопрос</a></li>
          <li><a href="/knowledge-base">База знаний</a></li>
          <li><a href="/rating">Рейтинг</a></li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-user"></span><b class="caret"></b></a>
            <ul class="dropdown-menu">
              <li><a href="/profile">Моя страница</a></li>
              <li><a href="/setting">Настройки</a></li>
              <li><a href="/logout">Выйти</a></li>
            </ul>
          </li>
        </ul>
      </div> <!-- /.nav-collapse -->
    </div> <!-- /.container -->
  </div> <!-- /.navbar -->

  <!-- 2-column layout -->
    <div class="container main-block">
      <div class="row row-offcanvas row-offcanvas-right">
    <!-- column 3 (sidebar) -->
        <div class="col-sm-3 sidebar-offcanvas" id="sidebar">
          <div class="list-group" role="navigation">
              <#list model.subjects as subject>
                  <#assign engSubject>${subject.engName}</#assign>
                  <a href="/main/${engSubject}" class="list-group-item">${subject.name}</a>
              </#list>
          </div>
        </div><!-- /column 3 (sidebar) -->

        <div class="col-xs-12 col-sm-9">
          <div class="row">
            <#list model.questions as question, subject>
              <#if subject.engName == model.currentSubject>
                <div class="col-sm-6">
                <!-- box 1 -->
                  <div class="panel panel-default">
                    <div class="panel-heading">
                      <h3 class="panel-title">${question.header}</h3>
                    </div>
                    <div class="panel-body">
                      <p>${question.content}</p>
                      <#assign idQ>${question.questionId}</#assign>
                      <p><a class="btn btn-default right" href="/question/${idQ}" role="button">Подробнее &raquo;</a></p>
                      <div class="row lead evaluation">
                        <div id="colorstar" class="starrr ratable" ></div>
                        <span id="count">0</span> Балл(ов)<span id="meaning"> </span>
                      </div>
                    </div>
                  </div>
                </div> <!-- /column 1 -->
              </#if>
            </#list>
          </div>
        </div>
      </div>
    </div>
  </body>
</html>