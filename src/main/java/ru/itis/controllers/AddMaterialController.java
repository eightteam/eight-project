package ru.itis.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import ru.itis.forms.MaterialForm;
import ru.itis.models.SubjectData;
import ru.itis.services.MaterialServiceImpl;
import ru.itis.utils.FileStorageUtil;
import ru.itis.validators.AddMaterialValidator;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * @author Dilyara Gabdreeva
 *         11-602
 *         <p>
 *         .2017
 */
@Controller
public class AddMaterialController {

    @Autowired
    private MaterialServiceImpl materialServiceImpl;

    @Autowired
    private AddMaterialValidator addMaterialValidator;

    @Autowired
    private FileStorageUtil fileStorageUtil;


    @InitBinder("materialForm")
    public void initAddMaterialValidator(WebDataBinder binder) {
        binder.addValidators(addMaterialValidator);
    }

    @GetMapping(value = "/add/{subject}")
    public String getAddMaterial(@PathVariable("subject") String subject, @ModelAttribute("model")ModelMap model) {
        model.addAttribute("subject", subject);
        return "add_material";
    }

    @PostMapping(value = "/add/{subject}")
    public String postAddMaterial(@ModelAttribute("materialForm") MaterialForm form, @RequestParam("file") MultipartFile file, @PathVariable("subject") String subject) {
        SubjectData material = new SubjectData();
        if (!form.getPath().equals("")) {
            material.setType(false);
            material.setPath(form.getPath());
            materialServiceImpl.saveMaterial(material, subject, form);
        }
        else {
            material = fileStorageUtil.convertSubjectDataFromMultipart(file);
            fileStorageUtil.copyToStorage(file, material.getFileName());
            material.setType(true);
            materialServiceImpl.saveMaterial(material, subject, form);
        }
        return "redirect:/knowledge-base";
    }
    @RequestMapping("/download/{fileName:.+}")
    public void downloadResource(HttpServletResponse response, @PathVariable("fileName") String fileName)
    {
        Path file = Paths.get(fileStorageUtil.getStoragePath() + "\\" +  fileName);
        if (Files.exists(file))
        {
            response.addHeader("Content-Disposition", "attachment; filename="+fileName);
            try
            {
                Files.copy(file, response.getOutputStream());
                response.getOutputStream().flush();
            }
            catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }
}