package ru.itis.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import ru.itis.models.Question;
import ru.itis.models.Subject;
import ru.itis.services.AuthenticationService;
import ru.itis.services.QuestionServiceImpl;
import ru.itis.services.SubjectServiceImpl;
import ru.itis.utils.FileStorageUtil;
import ru.itis.validators.AskFormValidator;

import java.util.List;

/**
 * @author Dilyara Gabdreeva
 *         11-602
 *         <p>
 *         .2017
 */
@Controller
public class AskController {

    @Autowired
    private AuthenticationService authenticationService;

    @Autowired
    private SubjectServiceImpl subjectServiceImpl;

    @Autowired
    private QuestionServiceImpl questionServiceImpl;

    @Autowired
    private AskFormValidator askFormValidator;

    @Autowired
    private FileStorageUtil fileStorageUtil;

    @InitBinder("askForm")
    public void initAddMaterialValidator(WebDataBinder binder) {
        binder.addValidators(askFormValidator);
    }


    @GetMapping("/ask")
    public String getAsk(@ModelAttribute("model") ModelMap model) {
        List<Subject> list = subjectServiceImpl.getSubjects();
        model.addAttribute("subjects", list);
        return "ask";
    }

    @PostMapping("/ask")
    public String postAsk(@ModelAttribute("askForm") Question question, Authentication authentication, @RequestParam("file") MultipartFile file) {
        if (!file.isEmpty()) {
            question = fileStorageUtil.convertQuestionFromMultipart(file, question);
            fileStorageUtil.copyToStorage(file, question.getFileName());
        }
        else {
            question.setQFile("");
        }
        questionServiceImpl.saveQuestion(question, authenticationService.getUserByAuthentication(authentication));
        return "redirect:/";
    }
}
