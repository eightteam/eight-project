package ru.itis.validators;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import ru.itis.models.User;
import ru.itis.repositories.UserRepository;

import java.util.Optional;


/**
 * @author Dilyara Gabdreeva
 *         11-602
 *         <p>
 *         .2017
 */
@Component
public class UserRegistrationFormValidator implements Validator {

    @Autowired
    private UserRepository userRepository;

    @Override
    public boolean supports(Class<?> aClass) {
        return aClass.getName().equals(User.class.getName());
    }

    @Override
    public void validate(Object o, Errors errors) {
        User userForm = (User) o;

        Optional<User> existedUser = userRepository.findOneByLogin(userForm.getLogin());

        if (existedUser.isPresent()) {
            errors.rejectValue("login", "existedLogin", "Такой логин уже существует");
        }

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "login", "requiredLogin", "Поле обязательно для заполнения");

        if (userForm.getLogin().length() < 4 || userForm.getLogin().length() > 32) {
            errors.rejectValue("login", "wrongLoginLength","Логин должен иметь не менее 4 и не более 32 символов");
        }


        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "passwordRequired","Required");

        if (userForm.getPassword().length() < 8 || userForm.getPassword().length() > 32) {
            errors.rejectValue("password", "wrongPasswordLength", "Пароль должен содержать более 8 символов");
        }
        if (!userForm.getConfirmPassword().equals(userForm.getPassword())) {
            errors.rejectValue("confirmPassword", "confirmPasswords","Пароли не совпадают");
        }

    }
}