package ru.itis.validators;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import ru.itis.forms.UserLoginForm;
import ru.itis.repositories.UserRepository;

/**
 * @author Dilyara Gabdreeva
 *         11-602
 *         <p>
 *         .2017
 */
@Component
public class UserLoginFormValidator implements Validator {

    @Autowired
    private UserRepository userRepository;

    private final PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

    @Override
    public boolean supports(Class<?> aClass) {
        return aClass.getName().equals(UserLoginForm.class.getName());
    }

    @Override
    public void validate(Object o, Errors errors) {
        UserLoginForm form = (UserLoginForm) o;

        if (!userRepository.findOneByLogin(form.getLogin()).isPresent() || !userRepository.findOneByLogin(form.getLogin()).get().getPassword().equals(passwordEncoder.encode(form.getPassword()))) {
            errors.rejectValue("login", "notFoundLogin","Логин или пароль введены неверно");
        }
    }
}
