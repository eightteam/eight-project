package ru.itis.utils;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;
import ru.itis.models.Question;
import ru.itis.models.SubjectData;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.UUID;

/**
 * @author Dilyara Gabdreeva
 *         11-602
 *         <p>
 *         .2017
 */
@Component
public class FileStorageUtil {

    @Value("${storage.path}")
    private String storagePath;

    public String getStoragePath(){
        return storagePath;
    }

    public void copyToStorage(MultipartFile file, String storageFIleName) {
        try {
            Files.copy(file.getInputStream(), Paths.get(storagePath, storageFIleName));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public SubjectData convertSubjectDataFromMultipart(MultipartFile file) {
        String originalFileName = file.getOriginalFilename();
        String storageName = createStorageName(originalFileName);
        String fileUrl = getUrlOfFile(storageName);
        return SubjectData.builder()
                .path(fileUrl)
                .fileName(storageName)
                .build();
    }
    public Question convertQuestionFromMultipart(MultipartFile file, Question question) {
        String originalFileName = file.getOriginalFilename();
        String storageName = createStorageName(originalFileName);
        String fileUrl = getUrlOfFile(storageName);
        question.setQFile(fileUrl);
        question.setFileName(storageName);
        return question;
    }
    private String getUrlOfFile(String storageName) {
        return storagePath  + "/" + storageName;
    }
    private String createStorageName(String originalFileName) {
        String extension = FilenameUtils.getExtension(originalFileName);
        String newFileName = UUID.randomUUID().toString();
        return newFileName + "." + extension;
    }
}
