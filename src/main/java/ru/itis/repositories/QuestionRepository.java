package ru.itis.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.itis.models.Question;
import ru.itis.models.Subject;
import ru.itis.models.User;

import java.util.List;

/**
 * @author Dilyara Gabdreeva
 *         11-602
 *         <p>
 *         .2017
 */
public interface QuestionRepository extends JpaRepository<Question, Long> {
    List<Question> getAllByQSubject(Subject subject);
    List<Question> findAllByUser(User user);
}
