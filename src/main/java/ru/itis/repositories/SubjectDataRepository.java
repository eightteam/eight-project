package ru.itis.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.itis.models.Subject;
import ru.itis.models.SubjectData;

import java.util.List;

/**
 * @author Dilyara Gabdreeva
 *         11-602
 *         <p>
 *         .2017
 */
public interface SubjectDataRepository extends JpaRepository<SubjectData, Long> {
    List<SubjectData> getAllBySubject(Subject subject);
}
