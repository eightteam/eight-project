package ru.itis.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.itis.models.Subject;

/**
 * @author Dilyara Gabdreeva
 *         11-602
 *         <p>
 *         .2017
 */
public interface SubjectRepository extends JpaRepository<Subject, Long> {
    Subject findByEngName(String engName);
    Subject findByName(String engName);
}
