package ru.itis.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.itis.models.Question;
import ru.itis.models.Subject;
import ru.itis.models.User;
import ru.itis.repositories.QuestionRepository;

import java.util.List;
import java.util.Set;

/**
 * @author Dilyara Gabdreeva
 *         11-602
 *         <p>
 *         .2017
 */
@Service
public class QuestionServiceImpl implements QuestionService {

    @Autowired
    private QuestionRepository questionRepository;

    @Autowired
    private SubjectServiceImpl subjectServiceImpl;

    public List<Question> getQuestions(String subject) {
        return questionRepository.getAllByQSubject(subjectServiceImpl.getSubjectByName(subject));
    }

    public void saveQuestion(Question question, User user) {
        Subject subject = subjectServiceImpl.getSubjectByNameRus(question.getSubjectName());
        System.out.println(subject.getName());
        Set<Question> questions = subject.getQuestionsSet();
        questions.add(question);
        subject.setQuestionsSet(questions);
        question.setUser(user);
        question.setQSubject(subject);
        questionRepository.save(question);
        subjectServiceImpl.saveSubject(subject);
    }
    public Question getQuestionById(Long id) {
        return questionRepository.findOne(id);
    }
}
