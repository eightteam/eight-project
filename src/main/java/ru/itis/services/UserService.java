package ru.itis.services;

import ru.itis.models.User;

/**
 * @author Dilyara Gabdreeva
 *         11-602
 *         <p>
 *         .2017
 */
public interface UserService {
    void updateUser(User user, User currentUser);
}
