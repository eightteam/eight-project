package ru.itis.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.itis.models.User;
import ru.itis.repositories.UserRepository;

/**
 * @author Dilyara Gabdreeva
 *         11-602
 *         <p>
 *         .2017
 */
@Service
public class UserServiceImpl implements UserService{

    @Autowired
    private UserRepository userRepository;

    private final PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

    public void updateUser(User user, User currentUser) {
        if (!user.getLogin().equals("")) {
            currentUser.setLogin(user.getLogin());
        }
        if (!user.getNickname().equals("")) {
            currentUser.setNickname(user.getNickname());
        }
        if (!user.getPassword().equals("")) {
            currentUser.setPassword(passwordEncoder.encode(user.getPassword()));
        }
        userRepository.save(currentUser);
    }
}
