package ru.itis.services;

import ru.itis.forms.MaterialForm;
import ru.itis.models.SubjectData;

import java.util.List;

/**
 * @author Dilyara Gabdreeva
 *         11-602
 *         <p>
 *         .2017
 */
public interface MaterialService {
    List<SubjectData> getMaterial(String subject);
    void saveMaterial(SubjectData material, String name, MaterialForm form);
}
