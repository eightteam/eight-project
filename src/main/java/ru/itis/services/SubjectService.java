package ru.itis.services;

import ru.itis.models.Subject;

import java.util.List;

/**
 * @author Dilyara Gabdreeva
 *         11-602
 *         <p>
 *         .2017
 */
public interface SubjectService {
    List<Subject> getSubjects();
    Subject getSubjectByName(String name);
    Subject getSubjectByNameRus(String name);
    void saveSubject(Subject subject);
}
