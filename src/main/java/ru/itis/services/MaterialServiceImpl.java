package ru.itis.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.itis.forms.MaterialForm;
import ru.itis.models.Subject;
import ru.itis.models.SubjectData;
import ru.itis.repositories.SubjectDataRepository;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author Dilyara Gabdreeva
 *         11-602
 *         <p>
 *         .2017
 */
@Service
public class MaterialServiceImpl implements MaterialService{

    @Autowired
    private SubjectDataRepository materialRepository;

    @Autowired
    private SubjectServiceImpl subjectServiceImpl;

    public List<SubjectData> getMaterial(String subject) {
        List<SubjectData> list = materialRepository.getAllBySubject(subjectServiceImpl.getSubjectByName(subject));
        return list;
    }
    public void saveMaterial(SubjectData material, String name, MaterialForm form){
        Subject subject = subjectServiceImpl.getSubjectByName(name);
        Set<SubjectData> materials;
        if (subject.getSubjectDataSet() == null) {
            materials = new HashSet<>();
        }
        else {
            materials = subject.getSubjectDataSet();
        }
        materials.add(material);
        subject.setSubjectDataSet(materials);
        material.setSubject(subject);
        material.setContent(form.getContent());
        material.setHeader(form.getHeader());
        materialRepository.save(material);
        subjectServiceImpl.saveSubject(subject);
    }

}

