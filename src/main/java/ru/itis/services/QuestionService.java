package ru.itis.services;

import ru.itis.models.Question;
import ru.itis.models.User;

import java.util.List;

/**
 * @author Dilyara Gabdreeva
 *         11-602
 *         <p>
 *         .2017
 */
public interface QuestionService {
    List<Question> getQuestions(String subject);
    void saveQuestion(Question question, User user);
    Question getQuestionById(Long id);
}
