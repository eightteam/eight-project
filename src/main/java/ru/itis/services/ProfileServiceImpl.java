package ru.itis.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.itis.models.Question;
import ru.itis.models.User;
import ru.itis.repositories.QuestionRepository;

import java.util.List;

/**
 * @author Dilyara Gabdreeva
 *         11-602
 *         <p>
 *         .2017
 */
@Service
public class ProfileServiceImpl implements ProfileService{

    @Autowired
    private QuestionRepository questionRepository;

    public List<Question> getAllQuestions(User user) {
        return questionRepository.findAllByUser(user);
    }
}
