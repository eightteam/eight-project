package ru.itis.dao;

import ru.itis.hibernate.HibernateConnector;
import ru.itis.models.Question;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * @author Dilyara Gabdreeva
 *         11-602
 *         <p>
 *         .2017
 */
public class QuestionDaoImpl extends SaveOrUpdateImpl<Question, Long> implements CrudDao<Question, Long>{

    private EntityManager entityManager;

    public QuestionDaoImpl() {}

    @Override
    public Question find(Long id) {
        entityManager = HibernateConnector.getEntityManagerFactory().createEntityManager();
        entityManager.getTransaction().begin();
        Question result =  entityManager.find(Question.class, id);
        entityManager.getTransaction().commit();
        return result;
    }

    @Override
    public void delete(Long id) {
        entityManager = HibernateConnector.getEntityManagerFactory().createEntityManager();
        entityManager.getTransaction().begin();
        Question q = entityManager.find(Question.class, id);
        entityManager.remove(q);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public List<Question> findAll() {
        TypedQuery<Question> query = entityManager.createQuery("SELECT w FROM Question w", Question.class);
        return query.getResultList();

    }
}
