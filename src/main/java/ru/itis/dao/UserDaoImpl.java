package ru.itis.dao;

import ru.itis.hibernate.HibernateConnector;
import ru.itis.models.User;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * @author Dilyara Gabdreeva
 *         11-602
 *         <p>
 *         .2017
 */
public class UserDaoImpl extends SaveOrUpdateImpl<User, Long> implements CrudDaoImpl<User, Long> {

    private EntityManager entityManager;

    public UserDaoImpl(){}

    @Override
    public List<User> findAll() {
        TypedQuery<User> query = entityManager.createQuery("SELECT w FROM User w", User.class);
        return query.getResultList();

    }

    @Override
    public User find(Long id) {
        entityManager = HibernateConnector.getEntityManagerFactory().createEntityManager();
        entityManager.getTransaction().begin();
        User result =  entityManager.find(User.class, id);
        entityManager.getTransaction().commit();
        return result;
    }

    @Override
    public void delete(Long id) {
        entityManager = HibernateConnector.getEntityManagerFactory().createEntityManager();
        entityManager.getTransaction().begin();
        User u = entityManager.find(User.class, id);
        entityManager.remove(u);
        entityManager.getTransaction().commit();
    }

}
