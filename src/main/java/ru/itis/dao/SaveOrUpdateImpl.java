package ru.itis.dao;

import org.springframework.stereotype.Repository;
import ru.itis.hibernate.HibernateConnector;

import javax.persistence.EntityManager;


/**
 * @author Dilyara Gabdreeva
 *         11-602
 *         <p>
 *         .2017
 */
@Repository
public abstract class SaveOrUpdateImpl<M, I> implements CrudDaoImpl<M, I>{

    private EntityManager entityManager;

    public void save(M model){
        entityManager = HibernateConnector.getEntityManagerFactory().createEntityManager();
        entityManager.getTransaction().begin();
        entityManager.persist(model);
        entityManager.getTransaction().commit();
    }

    @Override
    public void update(M model) {
        entityManager = HibernateConnector.getEntityManagerFactory().createEntityManager();
        entityManager.getTransaction().begin();
        entityManager.merge(model);
        entityManager.getTransaction().commit();
    }
}
